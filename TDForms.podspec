Pod::Spec.new do |s|
  s.name         = "TDForms"
  s.version      = "1.1.4"
  s.summary      = "iOS library for creating forms"
  s.description  = "The Distance iOS library for creating forms"
  s.homepage     = "https://bitbucket.org/thedistance/deprecated-tdforms"
  s.author       = { "The Distance" => "hello@thedistance.co.uk" }
  s.source       = { :git => "https://bitbucket.org/thedistance/deprecated-tdforms.git", :tag => "#{s.version}" }
  s.platform     = :ios, '5.0'
  s.source_files = 'TDForms/TDForms/*'
  s.requires_arc = true
end
