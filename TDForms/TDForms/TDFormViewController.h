//
//  TDFormViewController.h
//  CleverAccounts
//
//  Created by pw on 24/05/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDValidation.h"
#import "TDCell.h"
#import "TDButton.h"
#import "TDSubmitCell.h"
#import "TDLabelCell.h"
#import "TDTextFieldCell.h"
#import "TDTextField.h"
#import "TDInputAccessoryView.h"
#import "TDParentFormCell.h"
#import "TDButtonCell.h"
#import "TDTextViewCell.h"
#import "TDTextView.h"
#import "TDSwitchCell.h"
#import "TDPickerCell.h"
#import "TDDateCell.h"
#import "TDMultiLabelCell.h"
#import "TDListSelectViewController.h"
#import "TDImageSelectViewController.h"


@interface TDFormViewController : UITableViewController

@property (assign) BOOL scrolling;

- (void)dismissAllKeyboards;

- (void)refresh;

//for subclasses to override:
- (void)cellValueChanged:(TDCell*)cell;
- (void)formSubmitted:(NSDictionary*)form;
-(void)updateValues:(NSDictionary*)values;


//subclasses set these properties
@property (strong, nonatomic) NSArray *cells;
@property (strong, nonatomic) NSString *multipleErrorMessage UI_APPEARANCE_SELECTOR;


//other methods, library users can generally ignore
- (void)submit;
- (void)next:(TDCell*)fromCell;
- (void)prev:(TDCell*)fromCell;


@end
