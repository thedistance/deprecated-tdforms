//
//  TDMultiLabelCell.h
//  CleverAccounts
//
//  Created by pw on 13/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"

@interface TDMultiLabelCell : TDCell

+ (TDMultiLabelCell*)multiLabelCellWithKey:(NSString*)key;

@property (strong, nonatomic) UILabel *label1;
@property (strong, nonatomic) UILabel *label2;
@property (strong, nonatomic) UILabel *label3;
@property (strong, nonatomic) UILabel *label4;
@property (strong, nonatomic) UILabel *label5;
@property (strong, nonatomic) UILabel *label6;

@property (strong, nonatomic) UIColor *label1Color UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *label1Font UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *label2Color UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *label2Font UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *label3Color UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *label3Font UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *label4Color UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *label4Font UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *label5Color UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *label5Font UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *label6Color UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *label6Font UI_APPEARANCE_SELECTOR;

@end
