//
//  TDListSelectCell.m
//  CleverAccounts
//
//  Created by pw on 20/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDListSelectCell.h"
#import "TDFormViewController.h"

@implementation TDListSelectCell

+ (TDListSelectCell*)listSelectCellWithTitle:(NSString*)title values:(NSArray*)items multipleSelect:(BOOL)multipleSelect key:(NSString*)key
{
    TDListSelectCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.items = items;
    cell.key = key;
    cell.multipleSelect = multipleSelect;
    
    if (cell.multipleSelect == YES) {
        cell.value = [NSMutableArray array];
    }
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _valueWidthAsProportion = 0.5;
        _titleToValuePadding = 0;
        
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        
        self.valueLabel = [[UILabel alloc] init];
        self.valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueLabel.backgroundColor = [UIColor clearColor];
        self.valueLabel.textAlignment = NSTextAlignmentRight;
        self.valueLabel.numberOfLines = 0;
        self.valueLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.valueLabel];
        
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    self.valueLabel.textColor = self.textColor;
    self.valueLabel.font = self.font;
    
    if (self.titleColor) self.titleLabel.textColor = self.titleColor;
    if (self.titleFont) self.titleLabel.font = self.titleFont;
    if (self.valueColor) self.valueLabel.textColor = self.valueColor;
    if (self.valueFont) self.valueLabel.font = self.valueFont;
    
    if (self.cellSelectionColor != nil) {
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.contentView.bounds];
        self.selectedBackgroundView.backgroundColor = self.cellSelectionColor;
    }
    
    if (self.disclosureIndicatorView == nil) {
        
        if (self.disclosureIndicator == nil) self.disclosureIndicator = [[TDCell appearance] disclosureIndicator];
        
        self.disclosureIndicatorView = [[UIImageView alloc] initWithImage:self.disclosureIndicator];
        self.disclosureIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.disclosureIndicatorView];
        [self setNeedsUpdateConstraints];
    }
    
    if (self.disclosureIndicatorView.image == nil) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.disclosureIndicatorView onView:self.contentView];
    [self removeConstraintsForView:self.valueLabel onView:self.contentView];
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    
    if (self.disclosureIndicatorView != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.disclosureIndicatorView
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding-(self.disclosureIndicatorView.frame.size.width / 2.0)]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.disclosureIndicatorView
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
    }
    
    if (self.valueLabel != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueLabel
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeWidth
                                         multiplier:self.valueWidthAsProportion
                                         constant:0]];
        
        if (self.disclosureIndicatorView.image != nil) {
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeRight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.disclosureIndicatorView
                                             attribute:NSLayoutAttributeLeft
                                             multiplier:1
                                             constant:-self.rightPadding]];
        } else {
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeRight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeRight
                                             multiplier:1
                                             constant:-self.rightPadding]];
        }
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueLabel
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueLabel
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    if (self.titleLabel != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:-self.titleToValuePadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    
    [super updateConstraints];
}

- (BOOL)select {
    
    [self.formViewController dismissAllKeyboards];
    
    TDListSelectViewController *vc = [TDListSelectViewController listSelectForCell:self];
    
    [self.formViewController.navigationController pushViewController:vc animated:YES];
    
    return YES;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSString class]]) {
        self.valueLabel.text = value;
    }
}


@end
