//
//  TDButton.m
//  CleverAccounts
//
//  Created by pw on 06/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDButton.h"

@implementation TDButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = self.bounds;
}

@end
