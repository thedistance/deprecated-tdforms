//
//  TDListSelectViewController.m
//  CleverAccounts
//
//  Created by pw on 03/07/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDListSelectViewController.h"
#import "TDFormViewController.h"

@interface TDListSelectViewController ()

@property (strong) TDListSelectCell *parent;

@end

@implementation TDListSelectViewController

+ (TDListSelectViewController*)listSelectForCell:(TDListSelectCell*)parent {
    
    TDListSelectViewController *vc = [[self alloc] initWithStyle:UITableViewStylePlain];
    vc.parent = parent;
    return vc;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if ([self.navigationController.viewControllers count] > 1) {

        if (self.parent.backButtonImage != nil) {
            
            UIImage *backButtonImage = self.parent.backButtonImage;
            CGRect backButtonFrame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
            UIButton *backButton = [[UIButton alloc] initWithFrame:backButtonFrame];
            [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButton addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
            [backButton setShowsTouchWhenHighlighted:YES];
            
            UIBarButtonItem *backBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:backButton];
            
            self.navigationItem.leftBarButtonItem = backBarButtonItem;
        }
    }
    
    self.title = self.parent.titleLabel.text;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.parent.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    // Configure the cell...
    
    NSString *item = self.parent.items[indexPath.row];
    
    cell.textLabel.text = item;
    
    cell.textLabel.font = self.parent.font;
    cell.textLabel.textColor = self.parent.textColor;
    
    if (self.parent.multipleSelect) {
        
        bool found = NO;
        for (NSString *string in (NSMutableArray*)self.parent.value) {
            if ([string isEqualToString:self.parent.items[indexPath.row]]) {
                found = YES;
            }
        }
        
        if (found) {
            cell.accessoryView = [[UIImageView alloc] initWithImage:self.parent.selectImage];
        }
        else {
            cell.accessoryView = nil;
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.parent.multipleSelect) {
        
        int i = 0;
        int found = -1;
        for (NSString *string in (NSMutableArray*)self.parent.value) {
            if ([string isEqualToString:self.parent.items[indexPath.row]]) {
                found = i;
            }
            i++;
        }
        
        if (found >= 0) {
            [(NSMutableArray*)self.parent.value removeObjectAtIndex:found];
        }
        else {
            [(NSMutableArray*)self.parent.value addObject:self.parent.items[indexPath.row]];
        }
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [self.tableView reloadData];
    }
    else {

        self.parent.value = self.parent.items[indexPath.row];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    [self.parent.formViewController cellValueChanged:self.parent];
}

@end
