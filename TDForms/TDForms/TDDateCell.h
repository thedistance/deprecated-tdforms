//
//  TDDateCell.h
//  CleverAccounts
//
//  Created by pw on 25/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"

@interface TDDateCell : TDCell

+ (TDDateCell*)dateCellWithTitle:(NSString*)title value:(NSDate*)value key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UITextField *valueTextField;

@property (strong, nonatomic) UIDatePicker *datePicker;

@property (assign, nonatomic) float titleToValuePadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float valueWidthAsProportion UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) NSDateFormatter *formatter UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *backgroundImage UI_APPEARANCE_SELECTOR;

@end
