//
//  TDParentFormCell.h
//  CleverAccounts
//
//  Created by pw on 20/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"


@class TDParentFormCell;


@protocol TDChildViewController <NSObject>

@property (nonatomic, strong) TDParentFormCell *parent;

@end


@interface TDParentFormCell : TDCell

+ (TDParentFormCell*)parentFormCellWithTitle:(NSString*)title viewController:(UIViewController<TDChildViewController>*)viewController key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UITextField *valueTextField;

@property (strong, nonatomic) UIViewController<TDChildViewController> *viewController;
@property (strong, nonatomic) UIImageView *disclosureIndicatorView;

@property (strong, nonatomic) UIColor *titleColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *titleFont UI_APPEARANCE_SELECTOR;

@property (assign, nonatomic) float titleToValuePadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float valueWidthAsProportion UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *valueColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *valueFont UI_APPEARANCE_SELECTOR;

@end
