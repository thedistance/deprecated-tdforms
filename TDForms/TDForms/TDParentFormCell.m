//
//  TDParentFormCell.m
//  CleverAccounts
//
//  Created by pw on 20/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDParentFormCell.h"
#import "TDFormViewController.h"

@implementation TDParentFormCell

+ (TDParentFormCell*)parentFormCellWithTitle:(NSString*)title viewController:(UIViewController<TDChildViewController>*)viewController key:(NSString*)key
{
    TDParentFormCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.viewController = viewController;
    cell.key = key;
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _valueWidthAsProportion = 0.5;
        _titleToValuePadding = 0;
        
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        
        self.valueTextField = [[TDTextField alloc] init];
        self.valueTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueTextField.backgroundColor = [UIColor clearColor];
        self.valueTextField.textAlignment = NSTextAlignmentLeft;
        self.valueTextField.background = [UIImage imageNamed:@"subview"];
        self.valueTextField.userInteractionEnabled = NO;
        self.valueTextField.inputAccessoryView = [TDInputAccessoryView inputAccessoryViewForCell:self];
        
        [self.contentView addSubview:self.valueTextField];
        
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    
    if (self.titleColor) self.titleLabel.textColor = self.titleColor;
    if (self.titleFont) self.titleLabel.font = self.titleFont;
    
    if (self.cellSelectionColor != nil) {
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.contentView.bounds];
        self.selectedBackgroundView.backgroundColor = self.cellSelectionColor;
    }
    
    [self.contentView addConstraint:[NSLayoutConstraint
                                     constraintWithItem:self.valueTextField
                                     attribute:NSLayoutAttributeWidth
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:self.contentView
                                     attribute:NSLayoutAttributeWidth
                                     multiplier:self.valueWidthAsProportion
                                     constant:-self.disclosureIndicatorView.image.size.width-self.rightPadding]];
    
    [self.contentView addConstraint:[NSLayoutConstraint
                                     constraintWithItem:self.valueTextField
                                     attribute:NSLayoutAttributeRight
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:self.contentView
                                     attribute:NSLayoutAttributeRight
                                     multiplier:1
                                     constant:-self.rightPadding]];
    
    
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.valueTextField onView:self.contentView];
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    
    if (self.valueTextField != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    if (self.titleLabel != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueTextField
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:-self.titleToValuePadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    [super updateConstraints];
}

- (BOOL)select {
    
    [self.formViewController dismissAllKeyboards];
    
    self.viewController.parent = self;
    [self.formViewController.navigationController pushViewController:self.viewController animated:YES];
    
    return YES;
}

- (id)value {
    
    return self.valueTextField.text;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSString class]]) {
        self.valueTextField.text = value;
    }
}


@end
