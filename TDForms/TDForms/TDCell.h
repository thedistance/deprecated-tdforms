//
//  TDCell.h
//  CleverAccounts
//
//  Created by pw on 24/05/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TDFormViewController;

@interface TDCell : UITableViewCell

@property (nonatomic, strong) TDFormViewController *formViewController;

@property (assign) BOOL editable;

@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) id value;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSArray *validations;

@property (assign, nonatomic) float cellHeight UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *cellColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *cellSelectionColor UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *disclosureIndicator UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *textColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *font UI_APPEARANCE_SELECTOR;

@property (assign, nonatomic) float leftPadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float rightPadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float topPadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float bottomPadding UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) UIView *separator;

@property (assign, nonatomic) float bottomSeparatorHeight UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *bottomSeparatorColor UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float bottomSeparatorWidthAsProportion UI_APPEARANCE_SELECTOR;

+ (NSString*)uuid;

- (void)next;
- (void)prev;

- (void)done;
- (BOOL)select;

-(void)removeConstraintsForView:(UIView *) view onView:(UIView *) hostView;

@end
