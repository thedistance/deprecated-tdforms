//
//  TDValidation.h
//  CleverAccounts
//
//  Created by pw on 06/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDValidation : NSObject

+ (TDValidation*)validationWithRegex:(NSString*)regex message:(NSString*)message;
+ (TDValidation*)nonNullValidationWithmessage:(NSString*)message;

- (BOOL)validate:(NSString*)string;

@property (nonatomic, strong) NSString *regex;
@property (nonatomic, strong) NSString *message;

@end
