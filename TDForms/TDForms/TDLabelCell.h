//
//  TDLabelCell.h
//  CleverAccounts
//
//  Created by pw on 13/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDLabelCell.h"
#import "TDCell.h"

@interface TDLabelCell : TDCell <UIActionSheetDelegate>

typedef enum {
    TDLabelCellActionNone,
    TDLabelCellActionTelephone,
    TDLabelCellActionEmail,
    TDLabelCellActionWeb,
    TDLabelCellActionMap
} TDLabelCellAction;

+ (TDLabelCell*)labelCellWithTitle:(NSString*)title subtitle:(NSString*)subtitle value:(NSString*)value key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subtitleLabel;
@property (strong, nonatomic) UILabel *valueLabel;
@property (strong, nonatomic) NSString *theEmailAddress;
@property (strong, nonatomic) UIImageView *valueLabelBackgroundImageView;

@property (assign) TDLabelCellAction actionType;

@property (assign, nonatomic) float titleToSubtitlePadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float titleToValuePadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float valueWidthAsProportion UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *titleColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *titleFont UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIFont *titleFontWhenNoSubtitleOrValue UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *subtitleColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *subtitleFont UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *valueColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *valueFont UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *valueColorWhenActionable UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *valueLabelBackgroundImage UI_APPEARANCE_SELECTOR;

@end
