//
//  TDImageSelectViewController.m
//  CleverAccounts
//
//  Created by pw on 03/07/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDImageSelectViewController.h"
#import "TDFormViewController.h"

@interface TDImageSelectViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

@property (strong) TDImageSelectCell *parent;

@property (strong) UIImageView *imageView;

@property (nonatomic, strong) UIPopoverController *popover;

@end

@implementation TDImageSelectViewController

+ (TDImageSelectViewController*)imageSelectForCell:(TDImageSelectCell*)parent {
    
    TDImageSelectViewController *vc = [[self alloc] init];
    vc.parent = parent;
    return vc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if ([self.navigationController.viewControllers count] > 1) {
        
        if (self.parent.editButtonImage == nil) {
            
            UIBarButtonItem *backBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(popViewControllerAnimated:)];
            self.navigationItem.leftBarButtonItem = backBarButtonItem;
        }
        else {
            UIImage *backButtonImage = self.parent.backButtonImage;
            CGRect backButtonFrame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
            UIButton *backButton = [[UIButton alloc] initWithFrame:backButtonFrame];
            [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
            [backButton addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
            [backButton setShowsTouchWhenHighlighted:YES];
            
            UIBarButtonItem *backBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:backButton];
            
            self.navigationItem.leftBarButtonItem = backBarButtonItem;
        }
    }
    
    if (self.parent.editButtonImage == nil) {
        
        UIBarButtonItem *editBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editImage)];
        self.navigationItem.rightBarButtonItem = editBarButtonItem;
    }
    else {
        
        UIImage *editButtonImage = self.parent.editButtonImage;
        CGRect editButtonFrame = CGRectMake(0, 0, editButtonImage.size.width, editButtonImage.size.height);
        UIButton *editButton = [[UIButton alloc] initWithFrame:editButtonFrame];
        [editButton setBackgroundImage:editButtonImage forState:UIControlStateNormal];
        [editButton addTarget:self action:@selector(editImage) forControlEvents:UIControlEventTouchUpInside];
        [editButton setShowsTouchWhenHighlighted:YES];
        
        UIBarButtonItem *editBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:editButton];
        self.navigationItem.rightBarButtonItem = editBarButtonItem;
    }
    
    self.title = self.parent.titleLabel.text;
	
	if (self.parent.image == nil) {
        
        [self editImage];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.imageView != nil) {
        if ([self.imageView isDescendantOfView:self.view])
            [self.imageView removeFromSuperview];
    }
    self.imageView = [[UIImageView alloc] initWithImage:self.parent.image];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.frame = self.view.bounds;
    [self.view addSubview:self.imageView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) [self.popover dismissPopoverAnimated:YES];
}

- (void)editImage {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setDelegate:self];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
            [popover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            self.popover = popover;
        }
        else {
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
    }
    else {
        
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Choose From Gallery", @"Take New Photo", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        if (self.view.window != nil) {
          [popupQuery showInView:self.view.window];
        } else {
            [popupQuery showInView:self.parent.window];
        }
        
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
    if (buttonIndex == [actionSheet cancelButtonIndex])
        return;
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    
	if (buttonIndex == 0) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    else if (buttonIndex == 1) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        [popover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        self.popover = popover;
    }
    else {
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    self.parent.image = image;
    
    [self.parent.formViewController cellValueChanged:self.parent];
    
    self.imageView.image = image;
	
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) [self.popover dismissPopoverAnimated:YES];
}

@end
