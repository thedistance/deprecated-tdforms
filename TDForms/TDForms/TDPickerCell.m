//
//  TDPickerCell.m
//  CleverAccounts
//
//  Created by pw on 25/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDPickerCell.h"
#import <QuartzCore/QuartzCore.h>
#import "TDTextField.h"
#import "TDFormViewController.h"

@interface TDPickerCell() <UIActionSheetDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation TDPickerCell

+ (TDPickerCell*)pickerCellWithTitle:(NSString*)title values:(NSArray*)values key:(NSString*)key
{
    TDPickerCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.title = title;
    cell.values = values;
    cell.key = key;
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _valueWidthAsProportion = 0.5;
        _titleToValuePadding = 0;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
    
        self.valueTextField = [[TDTextField alloc] init];
        self.valueTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueTextField.backgroundColor = [UIColor clearColor];
        self.valueTextField.textAlignment = NSTextAlignmentLeft;
        self.valueTextField.userInteractionEnabled = YES;
        self.valueTextField.inputAccessoryView = [TDInputAccessoryView inputAccessoryViewForCell:self];
        [self.contentView addSubview:self.valueTextField];
        

        self.pickerView = [[UIPickerView alloc] init];
        
        [self.pickerView setDataSource: self];
        [self.pickerView setDelegate: self];
        self.pickerView.showsSelectionIndicator = YES;
        
        int row = 0;
        for (NSString *string in self.values) {
            if ([string isEqualToString:self.value]) {
                break;
            }
            row++;
        }
        if (row < [self.values count]) {
            [self.pickerView selectRow:row+1 inComponent:0 animated:NO];
        }
        else {
            self.valueTextField.text = @"";
        }
        
        self.valueTextField.inputView = self.pickerView;

    }
    return self;
}

-(void) setPickerIndex:(int)index {
    if (self.pickerView) {
        [self.pickerView selectRow:index inComponent:0 animated:NO];
        [self pickerView:self.pickerView didSelectRow:index inComponent:0];
    }
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    
    if (self.backgroundImage != nil) {
        self.valueTextField.background = self.backgroundImage;
    }
    
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.valueTextField onView:self.contentView];
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    
    if (self.valueTextField != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeWidth
                                         multiplier:self.valueWidthAsProportion
                                         constant:1]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
    }
    
    if (self.titleLabel != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueTextField
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:-self.titleToValuePadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    [super updateConstraints];
}

- (id)value {
    
    return self.valueTextField.text;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSString class]]) {
        self.valueTextField.text = value;
    }
}

- (BOOL)select {
    
    NSIndexPath *indexPath = [self.formViewController.tableView indexPathForRowAtPoint:self.center];
    
    [UIView animateWithDuration:0.3
                     animations: ^{
                         
                         [self.formViewController.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
                         
                     } completion: ^(BOOL finished){
                         
                         [self performSelector:@selector(doFirstResponder) withObject:nil afterDelay:0.1];
                     }
     ];
    
    return YES;
}

- (void)doFirstResponder {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.formViewController.scrolling = NO;
        [self.valueTextField becomeFirstResponder];
    });
}

- (void)done {
    
    [self.valueTextField resignFirstResponder];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (row == 0) self.valueTextField.text = @"";
    else self.valueTextField.text = self.values[row-1];
    
    [self.formViewController cellValueChanged:self];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.values count]+1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (row == 0) return @"";
    return self.values[row-1];
}

@end
