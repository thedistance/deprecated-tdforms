//
//  TDTextFieldCell.m
//  CleverAccounts
//
//  Created by pw on 24/05/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDTextFieldCell.h"
#import "TDTextField.h"
#import "TDInputAccessoryView.h"
#import <QuartzCore/QuartzCore.h>
#import "TDFormViewController.h"

@interface TDTextFieldCell() <UITextFieldDelegate>

@end

@implementation TDTextFieldCell

+ (TDTextFieldCell*)textFieldCellWithTitle:(NSString*)title placeholder:(NSString*)placeholder key:(NSString*)key
{
    TDTextFieldCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.textField.placeholder = placeholder;
    cell.key = key;
    cell.title = title;
    
    return cell;
}

+ (TDTextFieldCell*)textFieldCellWithTitle:(NSString*)title placeholder:(NSString*)placeholder value:(NSString *)value key:(NSString*)key
{
    TDTextFieldCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.textField.placeholder = placeholder;
    cell.key = key;
    cell.textField.text = value;
    cell.title = title;
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _textFieldWidthAsProportion = 0.5;
        _titleToTextFieldPadding = 16;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        
        self.textField = [[TDTextField alloc] init];
        self.textField.delegate = self;
        self.textField.inputAccessoryView = [TDInputAccessoryView inputAccessoryViewForCell:self];
        self.textField.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.textField];
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.textField onView:self.contentView];
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    
    if (self.textField) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textField
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeWidth
                                         multiplier:self.textFieldWidthAsProportion
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textField
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textField
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textField
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    if (self.titleLabel != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.textField
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.titleToTextFieldPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeHeight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeHeight
                                         multiplier:1.0
                                         constant:0]];
    }
    
    [super updateConstraints];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [textField resignFirstResponder];
    
    return NO;
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self.formViewController cellValueChanged:self];
}

- (id)value {
    
    return self.textField.text;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSString class]]) {
        self.textField.text = value;
    }
    else if ([value isKindOfClass:[NSNumber class]]) {
        self.textField.text = [value stringValue];
    }
}

- (BOOL)select {
    
    NSIndexPath *indexPath = [self.formViewController.tableView indexPathForRowAtPoint:self.center];

    [UIView animateWithDuration:0.3
                     animations: ^{
                         
                         [self.formViewController.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
                         
                     } completion: ^(BOOL finished){
                         
                         [self performSelector:@selector(doFirstResponder) withObject:nil afterDelay:0.1];
                     }
     ];

    return YES;
}

- (void)doFirstResponder {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.formViewController.scrolling = NO;
        [self.textField becomeFirstResponder];
    });
}

- (void)done {

    [self.textField resignFirstResponder];
}

@end
