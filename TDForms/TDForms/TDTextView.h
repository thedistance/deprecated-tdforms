//
//  TDTextView.h
//  CleverAccounts
//
//  Created by pw on 02/07/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDTextView : UITextView

@property (strong, nonatomic) UIImage *backgroundImage UI_APPEARANCE_SELECTOR;

@end
