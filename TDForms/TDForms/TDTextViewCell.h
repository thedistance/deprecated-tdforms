//
//  TDTextViewCell.h
//  CleverAccounts
//
//  Created by pw on 24/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"

@class TDTextView;

@interface TDTextViewCell : TDCell

+ (TDTextViewCell*)textViewCellWithTitle:(NSString*)title value:(NSString*)value key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) TDTextView *textView;

@property (strong, nonatomic) UIImageView *imgView;

@property (assign, nonatomic) float titleToTextViewPadding UI_APPEARANCE_SELECTOR;

@end
