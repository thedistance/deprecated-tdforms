//
//  TDCell.m
//  CleverAccounts
//
//  Created by pw on 24/05/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"
#import "TDFormViewController.h"

@interface TDCell()

@end

@implementation TDCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _cellHeight = 40;

        _leftPadding = 16;
        _rightPadding = 16;
        _topPadding = 8;
        _bottomPadding = 8;
        
        _font = [UIFont systemFontOfSize:14];

        self.separator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.separator.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.separator];
        
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)setCellHeight:(float)cellHeight
{
    if (_cellHeight != cellHeight) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.formViewController refresh];
        });
        
    }
    _cellHeight = cellHeight;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if (self.bottomSeparatorColor != nil) {
        self.formViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.formViewController.tableView setSeparatorColor:[UIColor clearColor]];
        self.separator.hidden = NO;
    }
    else {
        self.separator.hidden = YES;
    }

    if (self.cellColor != nil)
        self.backgroundColor = self.cellColor;
    
    self.separator.backgroundColor = self.bottomSeparatorColor;
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.separator onView:self.contentView];
    
    if (self.separator != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.separator
                                         attribute:NSLayoutAttributeHeight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                         attribute:NSLayoutAttributeNotAnAttribute
                                         multiplier:1
                                         constant:self.bottomSeparatorHeight]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.separator
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:1]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.separator
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeWidth
                                         multiplier:self.bottomSeparatorWidthAsProportion
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.separator
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterX
                                         multiplier:1
                                         constant:0]];
    }
    
    [super updateConstraints];
}

+ (NSString *)uuid {
    //use this to generate cell identifiers
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    return uuidString;
}

- (void)next {

    [self.formViewController next:self];
}

- (void)prev {

    [self.formViewController prev:self];
}

- (BOOL)select {
    
    //override to set text field as first responder, bring up picker etc
    
    return NO;
}

- (void)done {
    
    //override to dismiss keyboard/picker etc
    
    [self resignFirstResponder];
    
}

-(void)removeConstraintsForView:(UIView *) view onView:(UIView *) hostView
{
    if (view != nil) {
        NSMutableArray *toRemove = [NSMutableArray arrayWithCapacity:hostView.constraints.count];
        
        for (NSLayoutConstraint *constr in hostView.constraints) {
            if (constr.firstItem == view || constr.secondItem == view) {
                [toRemove addObject:constr];
            }
        }
        
        [hostView removeConstraints:toRemove];
    }
}

@end
