//
//  TDTextField.h
//  CleverAccounts
//
//  Created by pw on 06/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDTextField : UITextField

@property (strong, nonatomic) UIColor *backgroundColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *textColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *font UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIImage *backgroundImage UI_APPEARANCE_SELECTOR;
@property (assign, atomic) float textPadding UI_APPEARANCE_SELECTOR;

@end
